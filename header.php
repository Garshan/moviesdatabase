<?php
  require 'dbfunctions.php';
  session_start();
?>

<!--
 * * * * * * * * * * * * * * * * * *
 * v.2 -2019 - Blanvillain Christian - ESIG
 * v.1 -2017 - Henauer Fabien - CFPT
 * * * * * * * * * * * * * * * * * *
-->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="icon" href="ressources/logo.png" >
    <link href="bootstrap/bootstrap.css" rel="stylesheet">
    <link href="bootstrap/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title><?= $title ?></title>
  </head>
  <body>
    <div class="col-lg-12">
      
      <header>
        <table>
          <tr>
            <td>
              <a href="index.php"><img src="ressources/logo.png" alt="logo"></a></td>
            <td>
              <h1>Movies Database</h1>
            </td>
            <td>
              <nav>
                <?php if (isset($_SESSION["idUser"]) && $_SESSION["idUser"] > 0): ?>

                  <a href="logout.php"><?= $_SESSION["message"] ?></a><br>
                <?php else: ?>

                  <a href="login.php"><?= empty($_SESSION["message"])?"Login or register":$_SESSION["message"] ?></a><br>
                <?php endif; ?>
                
              </nav>
            </td>
          </tr>
        </table>
      </header>
